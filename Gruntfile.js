module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    // load npm tasks
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    // config
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      cssmin: {
        target: {
          files: [{
            'static/css/style.min.css': 'src/assets/css/style.css',
            'static/plugins/bootstrap/css/bootstrap.min.css': 'src/assets/plugins/bootstrap/css/bootstrap.css',
          }]
        }
      },
      imagemin: {
        dynamic: {
          files: [{
            expand: true,
            cwd: 'src/assets/img',
            src: ['**/*.{png,jpg,gif,ico}'],
            dest: 'static/img'
          }, {
            expand: true,
            cwd: 'src/assets/images',
            src: ['**/*.{png,jpg,gif,ico}'],
            dest: 'static/images'
          }]
        }
      },
      uglify: {
        target: {
          files: {}
        }
      },
      htmlmin: {
        dist: {
          options: {
            removeComments: true,
            collapseWhitespace: true
          },
          files: {
          }
        }
      },
      watch: {
        options: {
            livereload: true
        },
        css: {
          files: ['src/assets/css/*.css', 'src/assets/css/!*.min.css'],
          tasks: ['cssmin'],
          options: {
            spawn: false,
          }
        },
        img: {
          files: ['src/assets/img/**/*.{png,jpg,gif,ico}'],
          tasks: ['imagemin'],
          options: {
            spawn: false,
          }
        }
      },
    });

    function writeReleaseVersion() {
      let date = new Date()
      let releaseDate = date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate() + "" + date.getHours() + "" + date.getMinutes() + "" + date.getSeconds();
      let content = 'let RELEASE_VERSION = "' + releaseDate + '"';
      grunt.file.write('static/js/version.js', content);
    }
    grunt.registerMultiTask('releaseVersion', '', writeReleaseVersion());

    // register tasks
    grunt.registerTask('default', [
        'cssmin',
        'imagemin',
        'uglify',
        'htmlmin'
    ]);
}
