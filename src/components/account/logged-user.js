import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
Vue.use(VueLocalStorage)
const ROLE = {
  ADMIN: 0,
  USER: 1
}
export default class LoggedUser {
  static getLoggedUser () {
    const defaultUserInfo = {ClientId: '', id: '', role: '', username: '', token: ''}
    if (Vue.localStorage.get('loggedUser')) return JSON.parse(Vue.localStorage.get('loggedUser'))
    else return defaultUserInfo
  }

  static setLoggedUser (data) {
    Vue.localStorage.set('loggedUser', JSON.stringify(data))
  }

  static removeLoggedUser () {
    Vue.localStorage.remove('loggedUser')
    Vue.localStorage.remove('accessToken')
  }

  static isAdmin () {
    return this.getLoggedUser().role.id === ROLE.ADMIN
  }

  static isClient () {
    return this.getLoggedUser().role.id === ROLE.USER
  }

  // static canCreates () {
  //     return this.getLoggedUser().permission.canCreate
  // }
  //
  // static canEdits () {
  //     return this.getLoggedUser().permission.canEdit
  // }
}
