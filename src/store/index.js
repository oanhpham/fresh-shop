import Vue from 'vue'
import Vuex from 'vuex'
import * as Types from './types'
import {paginationPipe} from "../filters/paginationFilter";
import {auth} from "../http_common";
import {API_URL} from "../constant";
import {swal} from "vue-swal/src";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
    categories: [],
    cart: {},
    cartHistory: [],
    orderBy: '',
    pageNumber: 1,
    perPage: 25,
    currentPage: 1,
    pagesToShow: 3,
    categoryFilter: '',
    priceFilter: {
      min: '',
      max: ''
    },
    nameFilter: '',
    starFilter: '',
    oderProduct: [],
    wishlist: [],
    providers: [],
    advanceFilter: ''
  },
  getters: {
    cartOrder: state => state.cart,
    products: state => state.products,
    categories: state => state.categories,
    providers: state => state.providers,
    pageNumber: state => state.pageNumber,
    getCartHistory: state => state.cartHistory,
    paginate(state, getters) {
      return state.advanceFilter === 0 ? paginationPipe(state.products, {perPage:state.perPage, currentPage: state.currentPage}) : paginationPipe(getters.filterProducts, {perPage:state.perPage, currentPage: state.currentPage})
    },
    paginateByPriceLow (state) {
      return state.products.sort((a, b) => a.cost - b.cost)
    },
    paginateByPriceHeight (state) {
      return state.products.sort((a, b) => b.cost - a.cost)
    },
    totalItemCountAfterFilter(state, getters) {
      return getters.filterProducts.length;
    },
    filterProducts(state, getters) {
      let category = state.categoryFilter
      let name = state.nameFilter
      let priceFilter = state.priceFilter
      let products = state.products
      let productSort = state.advanceFilter === 2 ? getters.paginateByPriceHeight : state.advanceFilter === 3 ? getters.paginateByPriceLow: products;
      let productFilterByCategory = category ? products.filter(product => product.category.id === category) : productSort;
      let productFilterByName = name ? productFilterByCategory.filter(product => product.provider.providerName === name) : productFilterByCategory
      return !(priceFilter.min === '' && priceFilter.max === '') ? productFilterByName.filter(product => product.cost > priceFilter.min && product.cost < priceFilter.max) : productFilterByName
    },
  },
  mutations: {
    getCart(state, data) {
      state.cart = data
    },
    getCategories(state, data) {
      state.categories = data
    },
    getProviders(state, data) {
      state.providers = data
    },
    getProducts(state, data) {
     state.products = data
    },
    getCartHistory(state, data) {
      state.cartHistory = data
    },
    updatePageNumber (state, pageNumber) {
      state.pageNumber = pageNumber
    },
    updateAdvanceFilter (state, filter) {
      state.advanceFilter = filter
    },
    [Types.ADD_PRODUCT_TO_CART] (state, product) {
      const cartItemIndex = state.cart.findIndex(item => item.id === product.id);

      if(cartItemIndex < 0) {
        state.cart.push({...product, quantity: 1});
      } else {
        if(state.cart[cartItemIndex].quantity === 10) return void  0;
        state.cart[cartItemIndex].quantity++;
      }
    },
    [Types.REMOVE_PRODUCT_FROM_CART] (state, id) {
      const cartItemIndex =  state.cart.findIndex(item => item.id === id);

      state.cart.splice(cartItemIndex, 1);
    },
    [Types.INCREMENT_CART_ITEM_QUANTITY] (state, id) {
      const cartItemIndex =   state.cart.findIndex(item => item.id === id);
      window.console.log(state.cart[cartItemIndex]);
      if(state.cart[cartItemIndex].quantity === 10) return void  0;
      state.cart[cartItemIndex].quantity++;

    },
    [Types.DECREMENT_CART_ITEM_QUANTITY] (state, id) {
      const cartItemIndex =   state.cart.findIndex(item => item.id === id);
      if(state.cart[cartItemIndex].quantity === 1) return void  0;
      state.cart[cartItemIndex].quantity--;
    },
    [Types.PREV_PAGE] (state) {
      state.currentPage--;
    },
    [Types.NEXT_PAGE] (state) {
      state.currentPage++;
    },
    [Types.GO_PAGE] (state, payload) {
      state.currentPage = payload;
    },
    [Types.CATEGORY_FILTER] (state, category) {
      state.categoryFilter = category
    },
    [Types.RESET_CATEGORY_FILTER] (state) {
      state.category = ''
    },
    [Types.NAME_FILER] (state, name) {
      state.nameFilter = name
    },
    [Types.PRICE_FILTER] (state, priceRange) {
      state.priceFilter = {
        max: priceRange.max,
        min: priceRange.min
      }
    },
    [Types.STAR_FILER] (state, star) {
      state.starFilter = star
    }
  },
  actions: {
    listCategory(context) {
      auth().get(`${API_URL}/category`).then(response => {
        context.commit('getCategories',response.data.data)
      }).catch(e => console.log(e))
    },
    updatePageNumber (context, data) {
      context.commit('updatePageNumber', data)
    },
    listProduct(context) {
      auth().get(`${API_URL}/product`).then(response => {
        context.commit('getProducts',response.data.data)
      }).catch(e => console.log(e))
    },
    getDetailCart(context) {
      auth().get(`${API_URL}/cart`).then(response => {
        context.commit('getCart', response.data)
      }).catch(e => console.log(e))
    },
    getCartHistory(context) {
      auth().get(`${API_URL}/cart/cart-history`).then(response => {
        context.commit('getCartHistory', response.data)
      }).catch(e => console.log(e))
    },
    getProviders(context) {
      auth().get(API_URL + '/provider').then(response => {
        context.commit('getProviders', response.data)
      })
    },
    updateCart (context, handler) {
      auth().put(API_URL + '/cart', handler.payload).then(res => {
        context.commit('getCart', res.data)
        handler.callback(res)
      }).catch(e => {
        swal({
          text: 'CÓ MỘT VÀI LỖI ĐÃ XẢY RA !',
          icon: 'error'
        })
      })
    },
    updateCartDetail (context, handler) {
      auth().put(API_URL + '/cart/detail-info', handler.payload).then(res => {
        context.commit('getCart', res.data)
        handler.callback(res)
      }).catch(e => {
        swal({
          text: 'CÓ MỘT VÀI LỖI ĐÃ XẢY RA',
          icon: 'error'
        })
      })
    },
    removeItemFromCart (context, handler) {
      auth().put(API_URL + '/cart/remove-order', handler.payload).then(res => {
        console.log(res)
        context.commit('getCart', res.data)
        handler.callback(res.data)
      }).catch(e => {
        console.log(e)
        swal({
          text: 'CÓ MỘT VÀI LỖI ĐÃ XẢY RA',
          icon: 'error'
        })
      })
    },
    processingCart (context, callback) {
      auth().put(API_URL + '/cart/processing-order').then(res => {
        callback(res)
        context.dispatch('getCartHistory')
      }).catch(e => {
        swal({
          title: 'CÓ MỘT VÀI LỖI ĐÃ XẢY RA',
          text: 'làm ơn hãy kiểm tra lại',
          icon: 'error'
        })
      })
    },
    receivedOrder (context, handler) {
      auth().put(API_URL + '/cart/complete-order/'+ handler.id).then(res => {
        handler.callback(res)
      }).catch(e => {
        swal({
          title: 'CÓ MỘT VÀI LỖI ĐÃ XẢY RA',
          text: 'làm ơn hãy kiểm tra lại\n'+e.toString(),
          icon: 'error'
        })
      })
    },
    cancelOrder (context, handler) {
      auth().put(API_URL + '/cart/cancel-order/'+ handler.id).then(res => {
        console.log(res)
        handler.callback(res)
      }).catch(e => {
        swal({
          title: 'SCÓ MỘT VÀI LỖI ĐÃ XẢY RA',
          text: 'please check again\n'+e.toString(),
          icon: 'error'
        })
      })
    },
    updateUserInfo(context, handler) {
      auth().put(`${API_URL}/user/${handler.userId}`, handler.payload).then(res => {
        localStorage.setItem('loggedUser', JSON.stringify(res.data.data))
        handler.callback(res)
      })
    },
    getProductArrivals(context) {
      auth().get(`${API_URL}/product/new_arrivals`).then(res => {
        context.commit('getProducts', res.data)
      })
    },
    getOrderProductHistory(context, callback) {
      auth().get(API_URL + '/cart/order-product-history').then(res => {
        callback(res)
      }).catch(e => {
        swal({
          title: 'CÓ MỘT VÀI LỖI ĐÃ XẢY RA',
          text: 'làm ơn hãy kiểm tra lại\n'+e.toString(),
          icon: 'error'
        })
      })
    }
  }
}
)
