import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home'
import Shop from '@/pages/shop'
import Product from '@/components/Product'
import Blog from '@/components/Blog'
import Post from '@/components/Post'
import Cart from '@/pages/cart'
import Layout from '@/components/Layout'
import Login from '@/components/account/Login'
import Register from '@/components/account/Register'
import CheckOut from '@/pages/processing/index'
import OrderHistory from '@/pages/processing/OrderHistory'
import OrderProductHistory from '@/pages/products/History'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Layout,
      children:[
        {
          path:'/',
          component:Home,
          name:'Home'
        },
        {
          path:'/shop',
          component:Shop,
          name:'Shop'
        },
        {
          path:'/product/:id',
          component:Product,
          name:'Product'
        },
        {
          path:'/blog',
          component:Blog
        },
        {
          path:'/post',
          component:Post
        },
        {
          path:'/cart',
          component:Cart
        },
        {
          path: '/login',
          component: Login,
          meta: {
            reload: true
          }
        },
        {
          path: '/register',
          component: Register,
          meta: {
            reload: true
          }
        },
        {
          path: '/profile',
          name: 'profile',
          meta: {
            reload: true
          },
          // lazy-loaded
          component: () => import('@/components/account/Profile.vue')
        },
        {
          path: '/oauth2/redirect',
          name: 'social',
          meta: {
            reload: true
          },
          component: () => import('@/components/account/RedirectOauth.vue')
        },
        {
          path: '/checkout',
          name: 'checkout',
          component: CheckOut
        },
        {
          path: '/special-product',
          name: 'special-product',
          component: Post
        },
        {
          path: '/order-history',
          name: 'OrderHistory',
          component: OrderHistory
        },
        {
          path: '/order-product-history',
          name: 'OrderProductHistory',
          component: OrderProductHistory
        }
      ]

    }
  ],
    mode:'history'
},

  )
