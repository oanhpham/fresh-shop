import axios from 'axios/index'
import {ACCESS_TOKEN} from "./constant";
export const auth = function () {
  return axios.create({
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem(ACCESS_TOKEN)
    }
  })
}
